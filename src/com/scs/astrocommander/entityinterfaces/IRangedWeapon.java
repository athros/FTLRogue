package com.scs.astrocommander.entityinterfaces;

public interface IRangedWeapon {

	int getRange();
	
	int getShotValue();

}
