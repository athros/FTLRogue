package com.scs.astrocommander.entityinterfaces;

import com.scs.astrocommander.entities.mobs.AbstractMob;

public interface IUseable {

	void use(AbstractMob user);
}
