package com.scs.astrocommander.entityinterfaces;

public interface IMeleeWeapon extends ICarryable {

	int getMeleeValue();
	
}
