package com.scs.astrocommander.modules.inputmodes;

import com.googlecode.lanterna.input.KeyStroke;

public interface IInputHander {

	boolean processInput(KeyStroke ks);
	
}
